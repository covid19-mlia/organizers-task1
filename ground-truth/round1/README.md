# round1
Information Extraction task Covid-19 MLIA @ Eval Initiative


## Introduction ##

This repository contains annotations used to perform the evaluation of
system outputs during the first round of the information extraction task.

Reference is only available for English (both manual annotation and a
ROVER). For French and Swedish, there is no participant on those
languages. For German, Greek and Italian, we obtained submissions from
only one participant for each language (not the same participant for
all languages), making the ROVER process impossible.


## Files ##

Files:

* T1-R1-en_manual.tar.gz: gold standard annotations made on 9 files
  (the most annotated files by all participants) from the test dataset
  (only one human annotator, no inter-annotator agreement)

* T1-R1-en_rover-all.tar.gz: ROVER made on all submissions from all
  participants for the 52 files from the test dataset; a majority vote
  is considered

* T1-R1-en_rover-concat.tar.gz: first, all submissions from a given
  participant are merged into a single submission; second, the ROVER
  is produced on those merged submissions; this two steps method
  allows us to do not favour participants that would have submit
  several runs, making their predictions more often used in the
  majority vote process. The ROVER is computed on the 52 files from
  the test dataset; annotations made by half of the participants are
  finally kept

In the report written by the organizers of the first task, results are
evaluated w.r.t. both the ROVER produced on merged submissions and the
gold standard annotations.


## Evaluation tools ##

We used the BRATeval tool to compute results, using a relax mode
(which allows for an error of maximum one character for each computed
offset).

The following line code has been used:

	java -cp BRATEval-0.0.2-SNAPSHOT.jar au.com.nicta.csp.brateval.CompareEntities reference/ system/ false

