\documentclass{llncs}


\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[numbers,sort&compress]{natbib}
\usepackage{url}
\usepackage{multirow}

\renewcommand{\bibsection}{\section*{References}}



\begin{document}

{\let\thefootnote\relax\footnotetext{Copyright \textcopyright\ 2020-2021 for this paper by its authors. Use permitted under Creative Commons License Attribution 4.0 International (CC BY 4.0). \\
Covid-19 MLIA @ Eval Initiative, \url{http://eval.covid19-mlia.eu/}.}}

\title{Covid-19 MLIA Information Extraction Task Round 1 Presentation and Main Findings}


\author{Cyril Grouin\inst{1} \and Thierry Declerck\inst{2} \and Pierre Zweigenbaum\inst{1}}
\institute{
Universit\'e Paris-Saclay, CNRS, LIMSI, France\\
\url{{cyril.grouin,pz}@limsi.fr}
\and
DFKI, Germany\\
\url{thierry.declerck@dfki.de} 
}


\maketitle

\begin{abstract}
In this paper, we present the information extraction task proposed in
the first round of the Covid-19 MLIA @ Eval Initiative. During this
first round, we proposed to identify six categories of information
potentially relevant for the Covid-19 issue (sign or symptom and
disease, medical test, drug and treatment, legal rules, everyday life
actions, and findings), on texts available in seven languages
(English, French, German, Greek, Italian, Spanish, and Swedish). Since
no gold standard annotations were given, the participants reused their
existing tools and ressources. Four teams participated in this task.
\end{abstract}

%%%%%%%%%%%%%%%%
\section{Introduction}
The goal of the Information Extraction task is to identify medical
information in texts. We defined six major types of entities to be
identified. Those categories are mainly related to the Covid-19
issue. The main objective is to mine texts in order to access relevant
information concerning the Covid-19, and more specifically information
that may help the health professional to find outcomes.

The first round of this task started on October 23rd. During this
round, participants will only have access to unannotated data in a
plain text format. The evaluation will consist in a ROVER of system
outputs \cite{fiscus1997}. We encourage the participants to try
experimental methods and to submit several system outputs in order to
exchange different views during the discussion at the virtual
meeting. The submissions were due on November 27th.

Thirty-two teams registered from seventeen countries (Australia,
Botswana, Canada, China, France, India, Italy, Mexico, Pakistan,
Portugal, Saudi Arabia, Spain, Switzerland, Tunisia, Turkey, The
United Kingdom, The United States of America). Almost all participants
belong to a university.


%%%%%%%%%%%%%%%%
\section{Task and Corpora}
%%% Task
\subsection{Task description}
In this task, we focused on six categories of information related to
the Covid-19 issue:
%
\begin{itemize}
  \item {\bf drug names, treatments, general intervention:} this
    category concerns both commercial and generic names of drugs, as
    well as general intervention in the health domain; elements from
    this category usually come from advices from a professional
    (medical doctor, pharmacist) or from self-medication, e.g.,
    Posaconazole AHCL, Allegra, Fexofenadine HCL, Xarelto, quarantine
  \item {\bf signs, symptoms, diseases:} this category deals with
    medical problems and merges together all signs, symptoms, and
    diseases shortness of breath, extreme fatigue, fever, skin
    infection, weightloss
  \item {\bf findings, efficacy of treatments:} this category is more
    complex since it concerns all elements related to positive or
    negative effets of treatments, including non expected stuff
  \item {\bf tests:} this category concerns all tests performed to
    diagnose medical problems such as blood sample, physical exam,
    serological test
  \item {\bf behaviors, everyday life actions:} this category concerns
    all actions performed by each of us such as to wash one's hands,
    to cough into his elbow, to self-confine, use of face masks,
    physical distancing
  \item {\bf legal dispositions, regulations:} this category concerns
    all actions decided by local or national authorities (Government,
    Ministry, etc.), such as to download the employer certificate,
    list of authorized move, prolonged border closure, closure of
    educational institutions
\end{itemize}

All system outputs are expected to be in the
BRAT~\cite{stenetorp-2012eacl} annotation format (i.e., a tabular
*.ann file for each *.txt file, composed of three columns: (i) an
annotation ID, (ii) category, starting offset, ending offset, and
(iii) the corresponding text span). An example is shown below:

%\begin{verbatim}
\verb+T1     drug-trt 34 68      Irbesartan Hydrochlorothiazide BMS+

\verb+T2     sosy-dis 116 125    dizziness+
%\end{verbatim}

Since the evaluation takes into account the position of the extracted
spans in the texts, the participants must carefully check the offsets
they compute.


%%% Corpora
\subsection{Corpora}
The corpora used in the information extraction task are exactly the
same than those from the machine translation task. We decided to
propose the same content in order to allow the reuse of findings from
task 1 in the two other tasks. Nevertheless, this choice implies that
sentences extracted from the parallel sentences do not refer anymore
to a coherent document since pairs of sentences from the machine
translation task do not needs to keep the whole content of a single
document. This also implies that content is parallel in each language,
which can be helpful for participants to produce multilingual tools.

Corpora are available in seven languages: English, French, German,
Greek, Italian, Spanish, and Swedish. For each language, we proposed
up to 12 files of content. Each file is composed of one sentence per
line. As previously explained, sentences are independant between them
and do not compose a consistent content. Table~\ref{tab:stats}
presents the number of files available in each language from the
training and test datasets, as well as the total number of words and
sentences to process. For the test dataset, we decided to split the
files into smaller ones (for a maximum number of 2\,500 sentences per
file) in order to make it easier both the computation of offsets of
characters for each annotated span and the output evaluation; this
splitting process explains the apparent increase number of files
between train and test.
%
\begin{table}[h]
  \centering
  \setlength{\tabcolsep}{3pt}
  \begin{tabular}{|l|l|r|r|r|r|r|r|r|} \cline{3-9}
    \multicolumn{2}{c|}{} & English & French & German & Greek & Italian & Spanish & Swedish \\ \hline
    \multirow{3}{*}{Train} & Files & 12 & 12 & 12 & 10 & 12 & 12 & 9 \\ \cline{2-9}
    % & Sentences & 19\,410\,352 & 22\,579\,866 & 16\,129\,995& 16\,352\,167 & 18\,188\,538 & 22\,260\,615 & 13\,163\,847 \\ \hline \hline
    & Words & 19410k & 22579k & 16129k & 16352k & 18188k & 22260k & 13163k \\ \cline{2-9}
    & Sentences & 1004k & 1004k & 926k & 834k & 900k & 1028k & 806k \\ \hline
    \multirow{3}{*}{Test} & Files & 52 & 52 & 18 & 5 & 7 & 32 & 12 \\ \cline{2-9}
    % & Sentences & 98\,583 & 98\,583 & 11\,849 & 2\,830 & 5\,338 & 55\,279 & 9\,062 \\ \hline
    & Words & 1768k & 2141k & 198k & 55k & 108k & 1165k & 129k \\ \cline{2-9}
    & Sentences & 98k & 98k & 11k & 2830 & 5338 & 55k & 9062 \\ \hline
  \end{tabular}
  \caption{Number of files, words and sentences ('k' stands for kilo:
    19410k means 19,410,000 words) per language from the training
    and test datasets}
  \label{tab:stats}
\end{table}

Table~\ref{tab:sample} gives a few sentences from the file 2730.txt in
four languages (English, French, German, and Spanish). Note that some
sentences are parallel in several languages.
%
\begin{table}[h]
  \centering
  \setlength{\tabcolsep}{1.5pt}
  \begin{tabular}{|l|p{11cm}|} \hline
    \multirow{6}{*}{English} & More factsheets \\ \cline{2-2}
    & For more information check the ECDC website: https://www.ecdc.europa.eu/en/measles/facts/factsheet\\ \cline{2-2}
    & on the third to seventh day, the temperature may reach up to 41 $^\text{o}$C;\\ \cline{2-2}
    & Two doses of the vaccine are needed for maximum protection.\\ \cline{2-2}
    & The only protection against measles is vaccination.\\ \hline
    \multirow{6}{*}{French} & Autres fiches d'information \\ \cline{2-2}
    & Pour en savoir plus, consultez le site web de l'ECDC: https://www.ecdc.europa.eu/en/measles/facts/factsheet \\ \cline{2-2}
    & du troisième au septième jour, la température peut atteindre 41 $^\text{o}$C; \\ \cline{2-2}
    & Deux doses du vaccin sont nécessaires pour obtenir une protection maximale. \\ \cline{2-2}
    & La seule protection contre la rougeole est la vaccination. \\ \hline
    \multirow{7}{*}{German} & Bei dieser Person besteht auch das Risiko von Komplikationen. \\ \cline{2-2}
    & Dies wird das Antigen genannt. \\ \cline{2-2}
    & Immunität hält in der Regel über mehrere Jahre an, mitunter ein ganzes Leben lang. \\ \cline{2-2}
    & Wie Impfstoffe wirken \\ \cline{2-2}
    & Diese ,,Gemeinschaftsimmunität" kann nur funktionieren, wenn genügend Personen geimpft sind. \\ \hline
    \multirow{8}{*}{Spanish} & Para obtener más información, visite el sitio web del ECDC: https://www.ecdc.europa.eu/en/measles/facts/factsheet . \\ \cline{2-2}
    & entre el tercer y el séptimo día, la temperatura puede llegar hasta 41 $^\text{o}$C; \\ \cline{2-2}
    & El 30 \% de los niños y de los adultos infectados con sarampión pueden desarrollar complicaciones. \\ \cline{2-2}
    & La vacuna TV es segura y efectiva y tiene muy pocos efectos secundarios. \\ \cline{2-2}
    & La primera dosis se administra entre los 10 y 18 meses de edad en los países europeos. \\ \hline
  \end{tabular}
  \caption{Extract from the file 2730.txt in a few languages}
  \label{tab:sample}
\end{table}



%%%%%%%%%%%%%%%%
\section{Results}
\subsection{Submissions}
We received eleven submissions from four teams: two compagnies,
Accenture (USA) and Innoradiant Research Group (France); and two
academic teams, SWLab/University of Cagliari (Italy) and ZHAW/School
of Engineering (Switzerland). Despite gentle reminders to the other
participants, we did not receive any additional submissions. We
present in the table~\ref{tab:sub} the number of submissions per
language for each participant.
%
\begin{table}[h]
  \centering
  \setlength{\tabcolsep}{4.5pt}
  \begin{tabular}{|l|c|c|c|c|c|c|c|} \hline
    \multirow{2}{*}{Team} & DE & EL & EN & ES & FR & IT & SV \\
    & German & Greek & English & Spanish & French & Italian & Swedish \\ \hline
    Accenture & 0 & 0 & 1 & 0 & 0 & 0 & 0 \\ \hline
    Innoradiant & 0 & 0 & 2 & 0 & 0 & 0 & 0 \\ \hline
    SWLab & 0 & 0 & 1 & 0 & 0 & 2 & 0 \\ \hline
    ZHAW & 1 & 1 & 1 & 1 & 0 & 0 & 0 \\ \hline
  \end{tabular}
  \caption{Number of submissions per language for each participant}
  \label{tab:sub}
\end{table}

While we received several submissions for English, we did not received
any submission for two languages (French and Swedish), and we received
submissions from only one team for the three other languages (German,
Greek, and Italian).

Among all received submissions, we observed a small formatting error
for one team (use of tabulations instead of spaces between label,
beginning offset and ending offset) that we automatically correct in
order to perform the evaluation.

\subsection{Evaluation}
Since there is no ground truth for this first round, we planned to
evaluate the system outputs based on a ROVER computed on the outputs
provided by all participants, following the general method designed by
\citet{fiscus1997}. As done by \citet{rebholz-2011jbs}, we worked at
the character level to align all outputs and to compute the majority
vote.

Results are evaluated using the traditional metrics used in
information extraction: precision, recall, and F-measure. Those
metrics are used to compute scores for each entity
class. Nevertheless, since this first round is explotary, we chose to
mainly evaluate the system outputs using a precision, which allows us
to evaluate how a system performs well among all provided predictions.

\subsubsection{English}
For English, due to the low number of submissions and so as to check
whether a ROVER-based evaluation is still relevant, we proposed two
evaluations: first, using a ROVER, and second, using gold standard
annotations.

\paragraph{ROVER}
As initially planned, we produced a ROVER based on all submissions. In
order to do not favour the participants that submitted several runs,
which would potentially increase the weight of their predictions in
the produced reference, we first concatened all submissions from each
participant into a single submission file. We then produced a ROVER
based on four outputs, corresponding to the four participants. We kept
annotations when they were shared by at least two participants (we
present in table~\ref{tab:rover} a short sample of output alignements
and the annotation kept in the final column for the sequence
\emph{``the spread of COVID-19.''} found in the file 3693-aa.txt).
%
\begin{table}[h]
  \centering
  \setlength{\tabcolsep}{4.5pt}
  \begin{tabular}{|l|l|l|l|l|l|l|} \hline
    Offset & Character & Innoradiant & SWLab & ZHAW & Accenture & ROVER \\ \hline 
    849 & t &   O &   O &   O &   O &   O \\ \hline
    850 & h &   O &   O &   O &   O &   O \\ \hline
    851 & e &   O &   O &   O &   O &   O \\ \hline
    852 & SPACE &  O &   O &   O &   O &   O \\ \hline
    853 & s &   O &   O &   B-findings &  O &   O \\ \hline
    854 & p &   O &   O &   I-findings &  O &   O \\ \hline
    855 & r &   O &   O &   I-findings &  O &   O \\ \hline
    856 & e &   O &   O &   I-findings &  O &   O \\ \hline
    857 & a &   O &   O &   I-findings &  O &   O \\ \hline
    858 & d &   O &   O &   I-findings &  O &   O \\ \hline
    859 & SPACE &  O &   O &   O &   O &   O \\ \hline
    860 & o &   O &   O &   O &   O &   O \\ \hline
    861 & f &   O &   O &   O &   O &   O \\ \hline
    862 & SPACE &  O &   O &   O &   O &   O \\ \hline
    863 & C &   B-sosy-dis &  O &   B-sosy-dis &  O &   B-sosy-dis \\ \hline
    864 & O &   I-sosy-dis &  O &   I-sosy-dis &  O &   I-sosy-dis \\ \hline
    865 & V &   I-sosy-dis &  O &   I-sosy-dis &  O &   I-sosy-dis \\ \hline
    866 & I &   I-sosy-dis &  O &   I-sosy-dis &  O &   I-sosy-dis \\ \hline
    867 & D &   I-sosy-dis &  O &   I-sosy-dis &  O &   I-sosy-dis \\ \hline
    868 & - &   I-sosy-dis &  O &   I-sosy-dis &  O &   I-sosy-dis \\ \hline
    869 & 1 &   I-sosy-dis &  O &   I-sosy-dis &  O &   I-sosy-dis \\ \hline
    870 & 9 &   I-sosy-dis &  O &   I-sosy-dis &  O &   I-sosy-dis \\ \hline
    871 & . &   O &   O &   O &   O &   O \\ \hline
  \end{tabular}
  \caption{Sample of output produced by the ROVER: offset, character,
    predictions from each team (Innoradiant, SWLab, ZHAW, Accenture),
    and final annotation kept}
  \label{tab:rover}
\end{table}
%% perl outputs-alignment.pl round1/en/rov-rover/3693-aa.txt round1/en/rov-inno/3693-aa.ann round1/en/rov-swlab/3693-aa.ann round1/en/zhaw1/3693-aa.ann round1/en/acc-corr/3693-aa.ann | perl rover-production.pl

Table~\ref{tab:resEnROVER} presents the total number of predictions
for each team, after concatenation of predictions from all
submissions, and the results for each category and globally.
%
\begin{table}[h]
  \centering
  \setlength{\tabcolsep}{4pt}
  \begin{tabular}{|l|r|c|c|c|c|c|c|c|} \hline
    Team & Predictions & behav. & drugs & find. & legal & sosy & tests & overall \\ \hline
    Innoradiant & 52\,992 & 1.000 & .976 & .000 & .000 & .995 & .977 & .990 \\ \hline
    ZHAW & 57\,061 & 1.000 & .963 & .000 & 1.000 & .988 & .919 & .982 \\ \hline
    Accenture & 7\,010 & .000 & .076 & .000 & 1.000 & .022 & .000 & .034 \\ \hline
    SWLab & 170 & .000 & .000 & .000 & .000 & .004 & .105 & .004 \\ \hline
  \end{tabular}
  \caption{Number of predictions and results (precision) for each
    category (behavior, drugs/treatments, findings, legal rules, sign
    or symptoms/diseases, tests) and globally (overall) for each
    submission, using the ROVER (52~files)}
  \label{tab:resEnROVER}
\end{table}


\paragraph{Gold standard annotations}
We manually produced gold standard annotations for a selection of 9
files from the test dataset, chosing the most frequently annotated
files by the four participants, assuming those files are the most
relevant in the test dataset. Since a human annotation process was not
planned, only one annotator participated in this work (no
inter-annotator agreement may be computed), for a total number of 1740
annotations: 1173 signs, symptoms and diseases,\footnote{Among all
  signs, symptoms, and diseases identified in the test dataset, we
  annotated 588 occurrences (50.1\%) of \emph{COVID-19, Covid-19,
    covid19, COVID19, Coronavirus Disease 2019} forms, and 199
  occurrences (17.0\%) of \emph{coronavirus, Coronavirus}. Other
  annotations mainly concern \emph{fever, cough, anxiety, worried,
    GAD} (General Anxiety Disorder), etc.} 228~behavior and everyday
life actions, 160 legal rules, 132 drugs and treatments, 46~medical
tests, and only 1 findings.\footnote{We identified the following
  phrase: \emph{``only about 2\% of the population has developed
    antibodies''}.}

We performed the evaluation only on those nine files using the
BRATeval tool, using a relax evaluation mode, which allows for
distance errors of one character w.r.t.\ the offsets from the
reference. Table~\ref{tab:resEnGS} presents the total number of
predictions on those files and the results for each submission.
%
\begin{table}[h]
  \centering
  \setlength{\tabcolsep}{3.4pt}
  \begin{tabular}{|l|r|c|c|c|c|c|c|c|} \hline
    \multirow{2}{*}{Team} & \multirow{2}{*}{Predictions} & behavior & drugs & find. & legal & sosy & tests & \multirow{2}{*}{overall} \\ %\cline{3-8}
    & & (n=228) & (132) & (1) & (160) & (1173) & (46) & \\ \hline
    Innoradiant & 3893 & .447 & .197 & .000 & .000 & .720 & .196 & .564 \\ \hline
    ZHAW & 3796 & .088 & .189 & .000 & .031 & .398 & .304 & .305 \\ \hline
    Innoradiant (long) & 263 & .355 & .000 & .000 & .000 & .000 & .000 & .047 \\ \hline
    Accenture & 559 & .000 & .083 & .000 & .000 & .008 & .000 & .012 \\ \hline
    SWLab, run \#2 & 9 & .000 & .000 & .000 & .000 & .001 & .000 & .001 \\ \hline
    SWLab, run \#1 & 8 & .000 & .000 & .000 & .000 & .000 & .000 & .000 \\ \hline 
    SWLab, run \#3 & 9 & .000 & .000 & .000 & .000 & .000 & .000 & .000 \\ \hline 
  \end{tabular}
  \caption{Number of predictions and results (precision) for each
    category (behavior, drugs/treatments, findings, legal rules, sign
    or symptoms/diseases, tests) and globally (overall) for each
    submission, using the gold standard annotations (9~files). The
    number of annotations per category in the reference is presented
    between parentheses}
  \label{tab:resEnGS}
\end{table}




\subsubsection{German, Greek, and Italian}
For the three other languages, since we only received submissions from
one team for each language (SWLab for Italian, ZHAW for German and
Greek), we can not produce any ROVER and we can not perform any
large-scale evaluation for those submissions.


%%%%%%%%%%%%%%%%
\section{Discussion}
% methodes des participants

As shown in table~\ref{tab:rover}, the ROVER is based on the
predictions mostly produced by the participants. One main default of
this method is that a participant may have produced a relevant
annotation, but this annotation will not be kept in the ROVER output
since the other participants did not produced a prediction for this
span, or they used a distinct label. This has two consequences: first,
we miss this relevant prediction, which is a pity for an exploratory
information extraction task; second, the evaluation process will
consider this prediction as a false positive, and thus, it decreases
the precision value.

Nevertheless, a comparison between tables~\ref{tab:resEnROVER}
and~\ref{tab:resEnGS} shows that there is no difference in the final
ranking of the participants. Obviously, the observed difference of
number of predictions between each participant (a lot of predictions
for Innoradiant and ZHAW, a moderate number of predictions for
Accenture, and a very low number of predictions for SWLab) is also an
explanation for the similar ranking achieved using the two
evaluations.

In this task, we considered all predictions as being relevant in their
category if shared by several participants (ROVER) or common with the
gold standard annotation. While this information extraction task
mainly focused on the Covid-19 issue, one may argue the need for a
slightly better adapted predictions and annotations to this issue. As
an example, if the symptom \emph{anxiety} is found in a text, but this
anxiety is not related to the Covid-19, identifying and extracting
this information is no longer relevant for the general Covid-19
general purpose. This implies a better understanding of the texts to
process and it makes more complex the task itself.

\bibliographystyle{splncs04nat}
\bibliography{biblio}


\end{document}
